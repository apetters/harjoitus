/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.sun.javafx.scene.accessibility.Action;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.WebView;

/**
 *
 * @author antepettersson
 */
public class FXMLDocumentController implements Initializable {
    int index = 0, limit = 0;
    private ArrayList<String> history;
    
    private final String http="http://";
    
    @FXML
    private Button backButton;
    @FXML
    private Button forwardButton;
    @FXML
    private Button refreshButton;
    @FXML
    private Button activateButton;
    @FXML
    private WebView webViewer;
    @FXML
    private TextField addressField;
    @FXML
    private Button initializeButton;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        history = new ArrayList<>();
        webViewer.getEngine().load("http://www.google.com");
        addressField.setText("www.google.com");
        // TODO
    }    

    @FXML
    private void backButtonAction(ActionEvent event) {
        String url = webViewer.getEngine().getLocation();
        if (url.startsWith("http://"))
            url = url.substring(7);
        history.add(url);
        if (index==9){ //full list
            history.remove(0);
            index-=1;
        }
        index-=1;
        if (index==0) //first element in history
            backButton.setDisable(true);
        if(index<limit && index < 8)//not last element
            forwardButton.setDisable(false);
        String ad = history.get(index);
        webViewer.getEngine().load(http + ad);
        addressField.setText(ad);
    }

    @FXML
    private void forwardButtonAction(ActionEvent event) {
        //System.out.println("forward: index was" + index);
        index+=1;
        //System.out.println("index is now " + index);
        if (index>0) //not first element in history
            backButton.setDisable(false);
        if(index==limit || index == 9)//last element
            forwardButton.setDisable(true);
        //System.out.println("history has " + history.size() + "elements");
        String ad = history.get(index);
        //System.out.println("url is gonna be" + ad);
        webViewer.getEngine().load(http + ad);
        addressField.setText(ad);
        
    }

    @FXML
    private void refreshButtonAction(ActionEvent event) {
        String prevUrl = webViewer.getEngine().getLocation();
        String url = addressField.getText();
        if (!url.equals(prevUrl)){//new address
            if (prevUrl.startsWith("http://"))
                prevUrl=prevUrl.substring(7);
            history.add(prevUrl);
            if (index==9){//history was full
                history.remove(0);
            } else {
            index +=1;
            limit+=1;
            if (index>0)
                backButton.setDisable(false);
            }
        
        }
        if (url.equals("index.html")){
            webViewer.getEngine().load(getClass().getResource("index2.html").toExternalForm());
            initializeButton.setDisable(false);
            activateButton.setDisable(false);
        } else {
            webViewer.getEngine().load(http + addressField.getText());
            initializeButton.setDisable(true);
            activateButton.setDisable(true);
        }
    }

    @FXML
    private void activateButtonAction(ActionEvent event) {
        webViewer.getEngine().executeScript("document.shoutOut()");
    }

    @FXML
    private void checkEnter(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER){
            this.refreshButtonAction(new ActionEvent());
        }
         
    }

    @FXML
    private void initializeButtonAction(ActionEvent event) {
        webViewer.getEngine().executeScript("initialize()");
    }
    
}
