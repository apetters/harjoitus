/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import static java.lang.Math.round;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author antepettersson
 */
public class SodamaticFXMLController implements Initializable {
    double moneySliderValue;
    BottleDispenser pk = null;
    @FXML
    private Label label;
    @FXML
    private Button addMoneyButton;
    @FXML
    private TextArea textField;
    @FXML
    private Button buyButton;
    @FXML
    private Slider moneySlider;
    @FXML
    private ComboBox<Bottle> sodaComboBox;
    
    public void output(String data){
        textField.setText(textField.getText()+ '\n' + data);
    }
    
    public void addItem (Bottle a){
            sodaComboBox.getItems().add(a);
    }
    
    public double getMoneySliderValue(){
        //return (moneySliderValue);
        return moneySlider.getValue();
    }
    
    public void loadMachine(ArrayList<Bottle> pullot){
        for (Bottle p : pullot)
            addItem(p);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        pk=new BottleDispenser();
        loadMachine(pk.getPullot());
        //moneySliderValue=round(moneySlider.getValue()*10)/10;
        moneySliderValue=moneySlider.getValue()*100;
    }    

    @FXML
    private void addMoneyButtonAction(ActionEvent event) {
        pk.addMoney(this);
        moneySlider.setValue(0);
    }

    @FXML
    private void buyButtonAction(ActionEvent event) {
        Bottle selection=sodaComboBox.getValue();
        pk.buyBottle(this, selection);
    }

    @FXML
    private void moneySliderChangedAction(MouseEvent event) {
        //moneySliderValue=round(moneySlider.getValue()*10)/10;
        moneySliderValue=moneySlider.getValue()*100;
    }

    @FXML
    private void moneySliderChangedAction(DragEvent event) {
    }
    
}
