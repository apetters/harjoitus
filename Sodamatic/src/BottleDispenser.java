
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import javafx.collections.ObservableList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public class BottleDispenser {
    String outfile = "kuitti.txt";
    private int bottles;
    private double money;
    private ArrayList<Bottle> pullot;
    
    public ArrayList<Bottle> getPullot(){
        return pullot;
    }
    
    public void printReceipt(Bottle selection) throws IOException {
        
        BufferedWriter bw= new BufferedWriter(new FileWriter(outfile, true));
        try {
            bw.write("Kuitti: ");
            bw.newLine();
            bw.write(selection.getName() + " "+selection.getSize() + "l, hinta "+selection.getPrice());
            bw.newLine();
            bw.newLine();
            bw.close();
        }catch (FileNotFoundException ex){
            System.out.println("error writing receipt: File Not Found");
        }
    }
    
    public BottleDispenser() {
        bottles = 6;
        pullot = new ArrayList<Bottle>();
        money = 0;
        //for (int i=0; i<bottles; i++)
        //    pullot.add(new Bottle());
        pullot.add(new Bottle("Pepsi Max", "Pepsi", 0.3f, 0.5f, 1.8f));
        pullot.add(new Bottle("Pepsi Max", "Pepsi", 0.3f, 1.5f, 2.2f));
        pullot.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.3f, 0.5f, 2.0f));
        pullot.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.3f, 1.5f, 2.5f));
        pullot.add(new Bottle("Fanta Zero", "Coca-Cola", 0.3f, 0.5f, 1.95f));
        pullot.add(new Bottle("Fanta Zero", "Coca-Cola", 0.3f, 0.5f, 1.95f));    
    }
    
    public void loadMachine(SodamaticFXMLController inst){
        for (Bottle pullo : pullot)
            inst.addItem(pullo);
    }
    
    public void addMoney(SodamaticFXMLController inst) {
        double value = inst.getMoneySliderValue();
        money += value;
        DecimalFormat form = new DecimalFormat();
        inst.output("Klink! Lisättiin "+ form.format(value) + " euroa laitteeseen!");
    }
    
    /*public void printBottles(){
    //perinteinen indeksipohjainen ratkaisu
    for (int i=0; i<bottles;i++){
    System.out.println(Integer.toString(i+1) + ". Nimi: "+pullot.get(i).getName());
    System.out.println("\tKoko: " + pullot.get(i).getSize() + "\tHinta: " + pullot.get(i).getPrice());
    }
    }*/
    
    /*    public void printBottles(){
    // iteraattoripohjainen ratkaisuun
    int j=0;
    for (Iterator i = pullot.iterator(); i.hasNext();){
    ++j;
    Bottle pullo = (Bottle) i.next();
    System.out.println(j + ". Nimi: " + pullo.getName() );
    System.out.println("\tKoko: " + pullo.getSize() + "\tHinta: " + pullo.getPrice());
    }
    }*/
    
    public void printBottles(SodamaticFXMLController inst){
        //for each - pohjainen ratkaisu
        int j=0;
        for (Bottle pullo : pullot){
            ++j;
            inst.output(j + ". Nimi: " + pullo.getName() );
            inst.output("\tKoko: " + pullo.getSize() + "\tHinta: " + pullo.getPrice());
        }
    }
    
    public void buyBottle(SodamaticFXMLController inst, Bottle selection) {
        //this.printBottles(inst);
        //inst.output("Valintasi: ");
        //int selection;
        //Scanner select = new Scanner(System.in);
        //if (!select.hasNextInt()){
        //    inst.output("Väärä valinta!");
        //    return;
        //}
        //selection=select.nextInt();
        //selection--; 
        if (money < selection.getPrice())
            inst.output("Syötä rahaa ensin!");
        else if (bottles == 0)
            inst.output("Pullot loppu!");
        else {
            bottles--;
            money -= selection.getPrice();
            inst.output("KACHUNK! "+ selection.getName()  +" tipahti masiinasta!");
            try{
                printReceipt(selection);
            }catch (IOException ex){
                System.out.println("Error printing receipt: IOException");
            }
            //pullot.remove(selection);
        }
    }
    
    public void returnMoney(SodamaticFXMLController inst) {
        inst.output("Klink klink. Sinne menivät rahat! Rahaa tuli ulos "+String.format("%.2f", money)+"€");
        money = 0;
    }
}
    /*
    public void printMenu(SodamaticFXMLController inst){
        inst.output("\n*** LIMSA-AUTOMAATTI ***\n"
                + "1) Lisää rahaa koneeseen\n"
                + "2) Osta pullo\n"
                + "3) Ota rahat ulos\n"
                + "4) Listaa koneessa olevat pullot\n"
                + "0) Lopeta\n"
                + "Valintasi: ");
        }
}
*/