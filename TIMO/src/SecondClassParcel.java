/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author antepettersson
 */
public class SecondClassParcel extends Parcel{

    //Safe, unlimited distance, small
    private final static double maxWeight = 3.0;
    public SecondClassParcel(double weight)  throws OverweightException {
        super (2, "second class parcel", "Safe parcel, unlimitd distance", 0.3, 0.3, 0.3, weight, maxWeight, -1, 2.0, 0.01);
        if (weight>maxWeight)
            throw new OverweightException();
            
    }
    
  
    
    @Override
    @SuppressWarnings("empty-statement")
    public void bump(int gs) {
        //System.out.println("second class safety!");
        //Very safe, i.e. total protection
        ;
    }

    //10mm padding, max dimensions 300*300*300 mm 
}
