
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author antepettersson
 */
public class TimoSystem {
    private ArrayList<Smartpost> smartposts;
    private StringList cities;
    private PacketStorage packets;
    
    public PacketStorage getPackets(){
        return packets;
    }
    
    public ArrayList<Smartpost> getSmartposts(){
        return smartposts;
    }
    
    public ArrayList<String> getCities(){
        return cities;
    }
    
    public TimoSystem(){
        smartposts = new ArrayList<>();
        cities = new StringList();
        packets= new PacketStorage();
    }
    
    public void loadFromItella(){
        //Lataa tehtävänannossa annetusta osoitteesta XML-muotoiset itellan smartpostit TimoSystemiin
        Document docu;
        URL itella;
        
        try{
            itella = new URL("http://smartpost.ee/fi_apt.xml");
        
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            
            docu = db.parse(itella.openStream());
            
            NodeList nodes = docu.getElementsByTagName("place");
            for (int i=0; i< nodes.getLength(); i++){
                 Element e = (Element)nodes.item(i);
                 smartposts.add(new Smartpost(e)); //leverages the Smartpost(element) - constructor
            }
            
        }catch (MalformedURLException ex) {
            System.out.println("Malformed URL");
        }catch (ParserConfigurationException ex) {
            System.out.println("ParserConfigurationException parsing itella");
        }catch (IOException ex){
            System.out.println("IOException parsing itella");
        }catch (SAXException sax){
            System.out.println("SAXEXception parsing itella");
        }
        //populate list of cities
        for (Smartpost s : smartposts)
            if (!cities.containsString(s.getCity()))
                cities.add(s.getCity());
    }
    
    public String getNames(){
        String output = "";
        for (Smartpost s : smartposts){
            output = output + s.getName() + "\n";
        }
        return output;
    }
}
