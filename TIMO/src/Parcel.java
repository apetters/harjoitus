/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author antepettersson
 */
public abstract class Parcel extends Item {
    //a Parcel can be an Item too, a Parcel in a Parcel..
    private static final double paddingDensity = 16; //kg/m^3, i.e. one lb/f^3 which is typical for PU foam 
    protected Item item=null;
    private Smartpost from=null, to=null;
    private double maxLength, maxWidth, maxHeigth, maxWeight;
    private int maxDistance; //km, -1 means unlimited
    private double price; //euro
    private int classNumber;
    
    public abstract void bump(int gs);  //how many g:s the parcel is exposed to
                                        //method so antishock may be implemented in children
    
    public Parcel(int cn, String n, String d, double x, double y, double z, double w, double mw, int dist, double p, double padding){
        super (n, n+", contains: "+ d,false, x+padding,y+padding,z+padding, w+(x*y+y*z+x*z)*2*padding*paddingDensity);
        //the parcel is an item which is slightly larger and heavier than it's contents
        classNumber = cn;
        maxLength=x;
        maxWidth=y;
        maxHeigth=z;
        maxWeight=w;
        maxDistance=dist;
        price=p;
        
    }
    
      public int getClassNumber(){
        return classNumber;
    }

public Smartpost getFrom(){
    return from;
}

public void setFrom(Smartpost s){
    from = s;
}

public Smartpost getTo(){
    return to;
}

public void setTo(Smartpost s){
    to = s;
}

public boolean checkFit(double x, double y, double z){
    return (x<=maxLength && y<=maxWidth && z<= maxHeigth);
}

public boolean checkWeight(double w){
    return (w<=maxWeight);
}

public int getMaxDistance(){
    return maxDistance;
}
public double getPrice(){
    return price;
}

public Item getItem(){
    return item;
}

public void setItem(Item i){
    item = i;
}

}



