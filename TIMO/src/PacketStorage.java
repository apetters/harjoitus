
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author antepettersson
 */
public class PacketStorage {
    private ArrayList<Parcel> items;
    
    public int getNumber(){
        return items.size();
    }
    
    public ArrayList<Parcel> asArrayList(){
        return items;
    }
    
    public PacketStorage(){
        items = new ArrayList();
    }
    
    public void add(Parcel p){
    items.add(p);
    }
    
    public Parcel get(int index){
        return items.get(index);
    }
    
    public void remove(int index){
        items.remove(index);
    }
    
    public void clear(){
            items.clear();
    }
}
