/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author antepettersson
 */
public class Location {
    private double lat, lon;
    
    public Location(){
        lat = 0.0;
        lon = 0.0;
    }
    
    public Location (double a, double b){
        lat = a;
        lon = b;
    }

    public double getLon(){
        return lon;
    }
    
    public double getLat(){
        return lat;
    }
    
    public void setLon(double a){
        lon = a;
    }
    
    public void setLat(double b){
        lat = b;
    }
}
