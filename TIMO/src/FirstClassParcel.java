/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author antepettersson
 */
public class FirstClassParcel extends Parcel {
    //Fast, max distance 150km, all fragile items will break, max weight 1kg, price 2 euros
    //private final int classNumber;
    private final static double maxWeight = 1.0;
    public FirstClassParcel(double weight)  throws OverweightException {
        super (1, "first class parcel", "Fast parcel, 150km", 0.3, 0.2, 0.04, weight, maxWeight, 150, 2.0, 0.002);
        if (weight>maxWeight)
            throw new OverweightException();
            
    }
    
  
    
    @Override
    public void bump(int gs) {
        //no shock absorption
        this.item.bump();
    }

    //2mm padding, max dimensions 300*200*40 mm 
}
