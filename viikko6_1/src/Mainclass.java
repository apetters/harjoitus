
import static java.lang.Math.round;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Bank pankki = new Bank();
        boolean quit = false;
        int choice;
        Scanner choiceScan=new Scanner(System.in);
        while (!quit){  //käyttöliittymälooppi
        System.out.println("\n*** PANKKIJÄRJESTELMÄ ***\n"
                + "1) Lisää tavallinen tili\n"
                + "2) Lisää luotollinen tili\n"
                + "3) Tallenna tilille rahaa\n"
                + "4) Nosta tililtä\n"
                + "5) Poista tili\n"
                + "6) Tulosta tili\n"
                + "7) Tulosta kaikki tilit\n"
                + "0) Lopeta");
        System.out.print("Valintasi: ");
        choice=choiceScan.nextInt();
        switch(choice){
            case 1: //lisää tavallinen tili
            {
                String tilinumero;
                double rahat;
                System.out.print("Syötä tilinumero: ");
                tilinumero=choiceScan.next();
                System.out.print("Syötä rahamäärä: ");
                rahat=choiceScan.nextDouble();
//                System.out.println("Tilinumero: "+tilinumero);
//                System.out.println("Rahamäärä: "+round(rahat));
                pankki.addAccount(tilinumero, rahat);
            }
                break;
            case 2: //lisää luotollinen tili
            {
                String tilinumero;
                double rahat, luotto;
                System.out.print("Syötä tilinumero: ");
                tilinumero=choiceScan.next();
                System.out.print("Syötä rahamäärä: ");
                rahat=choiceScan.nextDouble();
                System.out.print("Syötä luottoraja: ");
                luotto = choiceScan.nextDouble();
 //               System.out.println("Tilinumero: "+tilinumero);
 //               System.out.println("Rahamäärä: "+round(rahat));
 //               System.out.println("Luotto: "+round(luotto));
                pankki.addCreditAccount(tilinumero, rahat, luotto);
            }
                break;
            case 3: //tallenna tilille rahaa
            {
                String tilinumero;
                double rahat;
                System.out.print("Syötä tilinumero: ");
                tilinumero=choiceScan.next();
                System.out.print("Syötä rahamäärä: ");
                rahat=choiceScan.nextDouble();
//                System.out.println("Tilinumero: "+tilinumero);
//                System.out.println("Rahamäärä: "+round(rahat));
                pankki.makeDeposit(tilinumero, rahat);
            }
                break;
            case 4: //Nosta tililtä
            {
                String tilinumero;
                double rahat;
                System.out.print("Syötä tilinumero: ");
                tilinumero=choiceScan.next();
                System.out.print("Syötä rahamäärä: ");
                rahat=choiceScan.nextDouble();
//                System.out.println("Tilinumero: "+tilinumero);
//                System.out.println("Rahamäärä: "+round(rahat));
                pankki.makeWithdrawal(tilinumero, rahat);
            }    
                break;
            case 5: //poista tili
            {
                String tilinumero;
                System.out.print("Syötä poistettava tilinumero: ");
                tilinumero=choiceScan.next();
//                System.out.println("Tilinumero: "+tilinumero);
                pankki.removeAccount(tilinumero);
            }
                break;
            case 6: //tulosta tili
            {
                String tilinumero;
                System.out.print("Syötä tulostettava tilinumero: ");
                tilinumero=choiceScan.next();
//                System.out.println("Tilinumero: "+tilinumero);
                pankki.printAccount(tilinumero);
            }    
                break;
            case 7: //tulosta kaikki tilit
//                System.out.println("Tulostaa kaiken");
                pankki.printAllAccounts();
                break;
            case 0: //lopeta
                quit=true;
                break;
            default:
                System.out.println("Valinta ei kelpaa.");
                break;
        }
        }
    }
    
}