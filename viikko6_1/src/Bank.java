
import static java.lang.Math.round;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public class Bank {
    ArrayList<Account> accounts = new ArrayList<Account>();
    
    void addAccount(String accountNumber, double balance){
       //System.out.println("Pankkiin lisätään: "+accountNumber+","+round(balance));
       accounts.add(new NormalAccount(accountNumber, balance));
    }
    
    void addCreditAccount(String accountNumber, double balance, double credit){
       //System.out.println("Pankkiin lisätään: "+accountNumber+","+round(balance)+","+round(credit));
        accounts.add(new CreditAccount(accountNumber, balance, credit));
    }
    
    void removeAccount(String accountNumber){
        //System.out.println("Tili poistettu.");
        for (Account a: accounts){
            if (a.getAccountNumber().equals(accountNumber)){
                accounts.remove(a);
                System.out.println("Tili poistettu.");
                break;
            }
        }
    }
    
    void makeDeposit(String accountNumber, double sum){
        //System.out.println("Talletetaan tilille: "+accountNumber+" rahaa "+round(sum));
        for (Account a: accounts){
            if (a.getAccountNumber().equals(accountNumber)){
                a.deposit(sum);
                break;
            }
        }
    }
    
    void makeWithdrawal(String accountNumber, double sum){
        //System.out.println("Nostetaan tililtä: "+accountNumber+" rahaa "+round(sum));
        for (Account a: accounts){
            if (a.getAccountNumber().equals(accountNumber)){
                a.withdraw(sum);
                break;
            }
        }
    }
    
    void printAccount(String accountNumber){
        //System.out.println("Etsitään tiliä: "+accountNumber);
        for (Account a: accounts){
            if (a.getAccountNumber().equals(accountNumber)){
                a.print();
                break;
            }
        }
    }
    
    void printAllAccounts(){
        System.out.println("Kaikki tilit:");
        for (Account a: accounts) a.print();
    }
}