
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipInputStream;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public class ReadAndWriteIO {
    private final String filename="input.txt";
    
    public ReadAndWriteIO(){
        
    }
    
    public void readZipFile() throws IOException{
        
        ZipInputStream zr= new ZipInputStream(new FileInputStream("zipinput.zip"));
        while(zr.getNextEntry()!=null){
            while(zr.available()==1){
                int c = zr.read();
                if (Character.isDefined(c))
                    System.out.print((char)c);
            }
            System.out.println();
            zr.closeEntry();
        }
        zr.close();
    }
    
    public void readFile() throws IOException{
        String input;
        
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            while ((input=br.readLine())!=null){
                System.out.println(input);
            }
            br.close();
        } catch (FileNotFoundException ex) {
            System.out.println( System.getProperty( "user.dir" ) );
            //Logger.getLogger(IOObject.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    public void readAndWrite(String infile, String outfile) throws IOException{
        String input;
        try{
            
            BufferedReader br = new BufferedReader(new FileReader(infile));
            BufferedWriter bw = new BufferedWriter(new FileWriter(outfile));
            while ((input=br.readLine())!=null){
                if (!input.trim().isEmpty() && input.length() < 30 && input.toLowerCase().contains("v")){
                bw.write(input);
                bw.newLine();
                }
            }
            br.close();
            bw.close();
            } catch (FileNotFoundException ex){
                System.out.println( System.getProperty( "user.dir" ) );
        }
    } 
}

