/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public class Pullo {
    private String name;
    private String manufacturer;
    private float total_energy;
    private float price;
    private float size;
    
    public Pullo(){
        name = "Pepsi Max";
        manufacturer = "Pepsi";
        total_energy = 0.3f;
        size = 0.5f;
        price = 1.80f;
    }
    
    public String getName(){
        return name;
    }
    
    public float getPrice(){
        return price;
    }
    
}
