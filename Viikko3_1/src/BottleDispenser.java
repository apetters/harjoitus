
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */
public class BottleDispenser {
    
    private int bottles;
    private double money;
    private ArrayList<Bottle> pullot;
    
    public BottleDispenser() {
        bottles = 6;
        pullot = new ArrayList<Bottle>();
        money = 0;
        //for (int i=0; i<bottles; i++)
        //    pullot.add(new Bottle());
        pullot.add(new Bottle("Pepsi Max", "Pepsi", 0.3f, 0.5f, 1.8f));
        pullot.add(new Bottle("Pepsi Max", "Pepsi", 0.3f, 1.5f, 2.2f));
        pullot.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.3f, 0.5f, 2.0f));
        pullot.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.3f, 1.5f, 2.5f));
        pullot.add(new Bottle("Fanta Zero", "Coca-Cola", 0.3f, 0.5f, 1.95f));
        pullot.add(new Bottle("Fanta Zero", "Coca-Cola", 0.3f, 0.5f, 1.95f));
    }
    
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    
    /*public void printBottles(){
    //perinteinen indeksipohjainen ratkaisu
    for (int i=0; i<bottles;i++){
    System.out.println(Integer.toString(i+1) + ". Nimi: "+pullot.get(i).getName());
    System.out.println("\tKoko: " + pullot.get(i).getSize() + "\tHinta: " + pullot.get(i).getPrice());
    }
    }*/
    
    /*    public void printBottles(){
    // iteraattoripohjainen ratkaisuun
    int j=0;
    for (Iterator i = pullot.iterator(); i.hasNext();){
    ++j;
    Bottle pullo = (Bottle) i.next();
    System.out.println(j + ". Nimi: " + pullo.getName() );
    System.out.println("\tKoko: " + pullo.getSize() + "\tHinta: " + pullo.getPrice());
    }
    }*/
    
    public void printBottles(){
        //for each - pohjainen ratkaisu
        int j=0;
        for (Bottle pullo : pullot){
            ++j;
            System.out.println(j + ". Nimi: " + pullo.getName() );
            System.out.println("\tKoko: " + pullo.getSize() + "\tHinta: " + pullo.getPrice());
        }
    }
    
    public void buyBottle() {
        this.printBottles();
        System.out.print("Valintasi: ");
        int selection;
        Scanner select = new Scanner(System.in);
        if (!select.hasNextInt()){
            System.out.println("Väärä valinta!");
            return;
        }
        selection=select.nextInt();
        selection--; 
        if (money < pullot.get(selection).getPrice())
            System.out.println("Syötä rahaa ensin!");
        else if (bottles == 0)
            System.out.println("Pullot loppu!");
        else {
            bottles--;
            money -= pullot.get(selection).getPrice();
            System.out.println("KACHUNK! "+ pullot.get(selection).getName()  +" tipahti masiinasta!");
            pullot.remove(selection);
        }
    }
    
    public void returnMoney() {
        System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos "+String.format("%.2f", money)+"€");
        money = 0;
    }
    
    public void printMenu(){
        System.out.print("\n*** LIMSA-AUTOMAATTI ***\n"
                + "1) Lisää rahaa koneeseen\n"
                + "2) Osta pullo\n"
                + "3) Ota rahat ulos\n"
                + "4) Listaa koneessa olevat pullot\n"
                + "0) Lopeta\n"
                + "Valintasi: ");
        }
    
    public int select(){
        int selection, retval = 0;
        Scanner select = new Scanner(System.in);
        if (!select.hasNextInt()){
            System.out.println("Väärä valinta!");
            return 1;
        }
        selection=select.nextInt();
        switch(selection){
            case 1:
                this.addMoney();
                break;
            case 2:
                this.buyBottle();
                break;
            case 3:
                this.returnMoney();
                break;
            case 4:
                this.printBottles();
                break;
            case 0:
                retval = -1;
                break;
            default:
                System.out.println("väärä valinta");
                retval = 1;
                break;
        }
        return retval;
    }
}

