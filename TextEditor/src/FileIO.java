
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
//import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author antepettersson
 */

public class FileIO {
    private File IOfile;
    //private String filename;
    
    public FileIO(File file){
        this.IOfile=file;
    }
 
    
    public String readFile() throws IOException{
        String input="", output="";
        
        try {
            BufferedReader br = new BufferedReader(new FileReader(IOfile));
            while ((input=br.readLine())!=null){
                output = output + input + '\n';
            }
            /*do{
                input=br.readLine();
                output = output + input;
            }while (input!=null);*/
            br.close();
            return output.trim();
        } catch (FileNotFoundException ex) {
            System.out.println( System.getProperty( "user.dir" ) );
            //Logger.getLogger(IOObject.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }
    
    public void writeFile(String data) throws IOException{
        //String input;
        try{
            
           // BufferedReader br = new BufferedReader(new FileReader(infile));
            BufferedWriter bw = new BufferedWriter(new FileWriter(IOfile));
            bw.write(data);
            bw.close();
            } catch (FileNotFoundException ex){
                System.out.println( System.getProperty( "user.dir" ) );
        }
    }
}