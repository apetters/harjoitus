
import java.util.ArrayList;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Line;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author antepettersson
 */
public class ShapeHandler {
    protected double lineStartX=0, lineStartY=0, lineEndX=0,lineEndY=0; //pahaa koodia kun public, korjataan..
    private boolean chooseFirst=true;
    
    public boolean getChooseFirst(){
            return chooseFirst;
    }
    
    public void setChooseFirst(boolean val){
        chooseFirst= val;
    }
    
    private ArrayList<Point> points;
    private static ShapeHandler instance = null;
    
    protected ShapeHandler(){
        points = new ArrayList();
        //what else is needed
    }
    
    public static ShapeHandler getInstance(){
        if (instance == null)
            instance=new ShapeHandler();
        return instance;
        
    }
    
    public Line newLine() {
        System.out.println("nyt piiretään viiva!");
        
        
        Line line = new Line();
        int i = points.size();
        Point start = points.get(i-2), stop = points.get(i-1);
        line.setStartX(start.getCircle().getLayoutX());
        line.setStartY(start.getCircle().getLayoutY());
        line.setEndX(stop.getCircle().getLayoutX());
        line.setEndY(stop.getCircle().getLayoutY());
        System.out.println(line);
        return line;
}
    
    public Point newPoint(MouseEvent event){
        //luodaan uusi Point, lisätään listaan ja palautetaan näyttöä varten;
        Point p = new Point(event);
        points.add(p);
        return p;
    }
    
    public ArrayList<Point> getPoints(){
        return points;
    }
    
}
