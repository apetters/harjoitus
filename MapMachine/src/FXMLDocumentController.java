/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 *
 * @author antepettersson
 */
public class FXMLDocumentController implements Initializable {
    private ShapeHandler sh;
    
    @FXML
    private Label label;
    
    private AnchorPane AnchorPane;
    @FXML
    private AnchorPane base;
    @FXML
    private Button button;
        
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        sh=ShapeHandler.getInstance();
    }
    
    @FXML public void baseMouseClicked(MouseEvent event) {
       
        Point p = sh.newPoint(event);
        //piirretään viiva
        if (sh.getPoints().size()>1){
            //piirretään viiva
            Line l = sh.newLine();
            base.getChildren().add(l);
        }
        base.getChildren().add(p.getCircle());
    }
    

//    public void mouseHandler(MouseEvent event){
//                System.out.println("Hei, olen piste! X: " + event.getSceneX() + " , Y: " + event.getSceneY());
//                ShapeHandler sh = ShapeHandler.getInstance();
//                if (sh.getChooseFirst()){ //first point
//                    sh.lineStartX=event.getSceneX();
//                    sh.lineStartY=event.getSceneY();
//                    sh.setChooseFirst(false);
//                    //erase old line if exists
//                    
//                } else { //second point
//                    sh.lineEndX=event.getSceneX();
//                    sh.lineEndY=event.getSceneY();
//                    sh.setChooseFirst(true);
//                    //draw a new line
//                    System.out.println("piirretään viiva pisteestä " + sh.lineStartX + "," + sh.lineStartY + " pisteeseen "
//                            + sh.lineEndX +"," +sh.lineEndY);
//                    System.out.println(Parent.class);
//                    
//                    
//                }
//            }
        
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        for (Point p : sh.getPoints())
            System.out.println("x: " + p.getCircle().getLayoutX() + " Y: " + p.getCircle().getLayoutY());
    }
    
}